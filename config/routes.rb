Rails.application.routes.draw do
  devise_for :users

  resources :users

  resources :posts do
    resources :comments
  end

  get "home/index"

  get "search" => "search#index"

  root to: "home#index"

  mount ActionCable.server => "/cable"
end
