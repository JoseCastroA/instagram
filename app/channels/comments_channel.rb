class CommentsChannel < ApplicationCable::Channel
  def subscribed
    stream_from "comments"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def comment(data)
    ActionCable.server.broadcast("comments", comment: render_comment(data["comment"], data["post"]))
  end

  private

  def render_comment(comment, post_id)
    post = Post.find(post_id)
    time = Time.now
    post.comments.create(body: comment, user: current_user, created_at: time)
    ApplicationController.render(
      partial: "comments/comment",
      locals: {
        comment: comment,
        user: current_user,
        time: time,
      },
    )
  end
end
