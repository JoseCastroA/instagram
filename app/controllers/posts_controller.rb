class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    @post.user = current_user
    if @post.save
      flash[:notice] = "Post has been created"
      redirect_to user_path(current_user)
    else
      flash.now[:alert] = "Post has not been created"
      render :new
    end
  end

  def show
    @comment = @post.comments.build
    @comments = @post.comments.order(created_at: :desc)
  end

  def index
    @posts = Post.order(created_at: :desc)
  end

  def destroy
    unless @post.user == current_user
      flash[:alert] = "You can only delete your own post."
      render :show
    else
      if @post.destroy
        flash[:notice] = "Post has been deleted."
        redirect_to user_path(current_user)
      end
    end
  end

  private

  def post_params
    params.require(:post).permit(:description, :image, :user_id)
  end

  def set_post
    @post = Post.find(params[:id])
  end
end
