class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def show
    @posts = @user.posts.order(created_at: :desc)
  end

  def edit
  end

  def update
    if @user.update(user_params)
      flash[:notice] = "User has been updated"
      redirect_to user_path(current_user)
    else
      flash.now[:alert] = "User has not been updated"
      render :edit
    end
  end

  private

  def user_params
    params.require(:user).permit(:username, :name, :website, :bio, :email, :phone, :gender, :avatar)
  end

  def set_user
    @user = User.find(params[:id])
  end
end
