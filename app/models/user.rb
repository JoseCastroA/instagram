class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :validatable

  validates_presence_of :username

  has_many :posts, dependent: :destroy

  has_one_attached :avatar

  validates :email, presence: true
  validates :username, presence: true

  #mount_uploader :avatar, AvatarUploader
end
