require "rails_helper"
RSpec.feature "Editing user profile" do
  before do
    @jose = User.create!(username: "Jose", email: "jose@example.com", password: "password")
    visit "/"
    click_link "Sign in"
    fill_in "Email", with: @jose.email
    fill_in "Password", with: @jose.password
    click_button "Log in"
  end

  scenario "A user edit his profile" do
    visit "/"
    click_link @jose.username
    click_link "Edit profile"
    fill_in "Name", with: "Jose Castro Arias"
    fill_in "Website", with: "Kommit.co"
    fill_in "Phone", with: "3216928246"
    fill_in "Gender", with: "Masculino"
    click_button "Edit"
    expect(page).to have_content("User has been updated")
    expect(current_path).to eq(user_path(@jose))
    expect(page).to have_content(@jose.name)
    expect(page).to have_content(@jose.website)
  end

  scenario "A user fails to edit his profile" do
    visit "/"
    click_link @jose.username
    click_link "Edit profile"
    fill_in "Username", with: ""
    fill_in "Email", with: ""
    click_button "Edit"
    expect(page).to have_content("User has not been updated")
    expect(page).to have_content("Email can't be blank")
    expect(page).to have_content("Username can't be blank")
  end
end
