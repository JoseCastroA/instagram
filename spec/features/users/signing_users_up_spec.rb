require "rails_helper"

RSpec.feature "Users signup" do
  scenario "with valid credentials" do
    visit "/"
    click_link "Sign up"
    fill_in "Username", with: "Jose"
    fill_in "Email", with: "jose@example.com"
    fill_in "Password", with: "password"
    fill_in "Confirm password", with: "password"
    click_button "Sign up"
    expect(page).to have_content("You have signed up successfully.")
    expect(current_path).to eq(root_path)
  end

  scenario "with invalid credentials" do
    visit "/"
    click_link "Sign up"
    fill_in "Email", with: ""
    fill_in "Password", with: ""
    fill_in "Confirm password", with: ""
    click_button "Sign up"
    expect(page).to have_content("Email can't be blank")
    expect(page).to have_content("Username can't be blank")
    expect(page).to have_content("Password can't be blank")
  end
end
