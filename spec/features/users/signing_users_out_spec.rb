require "rails_helper"
RSpec.feature "Signing out signed-in users" do
  before do
    @jose = User.create!(username: "Jose", email: "jose@example.com", password: "password")
    visit "/"
    click_link "Sign in"
    fill_in "Email", with: @jose.email
    fill_in "Password", with: @jose.password
    click_button "Log in"
  end

  scenario do
    visit "/"
    click_link "Sign out"
    expect(page).to have_content("Signed out successfully.")
    expect(current_path).to eq(root_path)
    expect(page).not_to have_content("Sign out")
    expect(page).not_to have_content("#{@jose.username}")
  end
end
