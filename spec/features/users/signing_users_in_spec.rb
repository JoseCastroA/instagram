require "rails_helper"
RSpec.feature "Users signin" do
  before do
    @jose = User.create!(username: "Jose", email: "jose@example.com", password: "password")
  end
  scenario "with valid credentials" do
    visit "/"
    click_link "Sign in"
    fill_in "Email", with: @jose.email
    fill_in "Password", with: @jose.password
    click_button "Log in"
    expect(page).to have_content("Signed in successfully.")
    expect(page).to have_content("#{@jose.username}")
    expect(page).not_to have_link("Sign in")
    expect(page).not_to have_link("Sign up")
  end

  scenario "with no valid credentials" do
    visit "/"
    click_link "Sign in"
    fill_in "Email", with: "Jose@error.com"
    fill_in "Password", with: "Password-error"
    click_button "Log in"
    expect(page).to have_content("Invalid Email or password.")
    expect(page).to have_link("Sign in")
    expect(page).to have_link("Sign up")
  end
end
