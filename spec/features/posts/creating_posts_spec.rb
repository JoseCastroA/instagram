require "rails_helper"

RSpec.feature "Creating Posts" do
  before do
    @jose = User.create(username: "Jose", email: "jose@example.com", password: "password")
    login_as(@jose)
  end

  scenario "A user create a new post" do
    visit "/"
    click_link @jose.username
    click_link "Create post"
    fill_in "description", with: "Nueva imagen"
    attach_file "fileUploader", "#{Rails.root}/app/assets/images/image.jpg"
    click_button "Create post"
    expect(Post.last.user).to eq(@jose)
    expect(current_path).to eq(user_path(@jose))
    expect(page).to have_content("Post has been created")
    # Preguntar cómo verificar que la imagen se está mostrando correctamente
    # Preguntar cómo dar click o llenar un campo que está invisible
  end

  scenario "A user fails to create a new post" do
    visit "/"
    click_link @jose.username
    click_link "Create post"
    fill_in "description", with: ""
    click_button "Create post"
    expect(page).to have_content("Post has not been created")
    expect(page).to have_content("Description can't be blank")
    expect(page).to have_content("Image can't be blank")
  end
end
