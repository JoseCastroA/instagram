# README

Ruby version: 2.5.3

### Creando proyecto
```bash
rails _5.2.2_ new instagram –d postgresql
```
Dependencias
```
// Gemfile
// Añadir
gem 'pg'
gem 'rails_12factor', group: :production

gem "jquery-rails"
gem "bootstrap", "~> 4.1.1"
gem "font-awesome-rails"
gem "carrierwave"
gem "cloudinary"
gem "kaminari"

// Borrar
gem 'sqlite3'
```
Instalando dependencias
```bash
bundle install
```
Creando la base de datos
```bash
rails db:create
```
Ejecutando migraciones
```bash
rails db:migrate
```

Creando controladores
```bash
rails g controller controller_name index
```

Creando modelos
```bash
rails g model model_name atribute_name:type
```

##### Autenticación
Añadir la siguiete gema
```bash
gem 'devise', '~>4.5.0'
```
Generando los componentes de la gema
```bash
rails g devise:install
```
Generando el modelo de usuario
```bash
rails g devise user
```
Generando vistas del auth
```bash
rails g devise:views
```

### Añadiendo rspec para pruebas
Añadir gemas
```bash
gem "rspec-rails", "~> 3.8", group :development, :test do
gem "capybara", ">= 2.15", group :test do
```
Instalando dependencias
```bash
bundle install
```
Inicializando el directorio spec
```bash
rails generate rspec:install
```
Crear carpeta features para almacenar las pruebas
```bash
mkdir spec/features
```
Correr las pruebas
```bash
rspec or bundle exec rspec
```

### Creando guard
Añadiendo gemas
```bash
group :development do
  gem "guard"
  gem "guard-rspec"
  gem "guard-cucumber"
```
Instalando dependencias
```bash
bundle install
```
Añadiendo binstub
```bash
bundle binstubs guard
```
Inicializando guard
```bash
guard init
bundle exec cucumber --init or cucumber --init
```
Ejecutando guard
```bash
guard
```

### Ejecutando proyecto
Instalando dependencias
```bash
bundle install
```
Ejecutando pruebas
```bash
rspec
```
Ejecutando
```bash
rails server
```